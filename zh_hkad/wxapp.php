<?php
/**
 * 还康科技活动报名
 *
 * @author 指尖营销
 * @url http://bbs.we7.cc/
 */
defined('IN_IA') or exit('Access Denied');

class Zh_hkadModuleWxapp extends WeModuleWxapp {
	/**
	*课程分类
	**/
	public function doPageCoursecate(){
		global $_GPC, $_W;
		$res = pdo_getall('zh_jysp_course_cate',array('uniacid'=>$_W['uniacid']),array(),'','sort asc');
		if (empty($res)) {
			echo json_encode(array('msg'=>'暂无数据','code'=>'500'),320);exit();
		}
		echo json_encode(array('msg'=>'获取成功','code'=>'200','list'=>$res),320);exit();
	}
	/**
	*课程列表
	**/
	public function doPageCourse(){
		global $_GPC, $_W;
		$res = pdo_getall('zh_jysp_course',array('uniacid'=>$_W['uniacid']),array(),'','sort asc');
		if (empty($res)) {
			echo json_encode(array('msg'=>'暂无数据','code'=>'500'),320);exit();
		}
		echo json_encode(array('msg'=>'获取成功','code'=>'200','list'=>$res),320);exit();
	}

	/**
	*发送手机验证码
	**/
	public function doPageCode(){
	    global $_GPC,$_W;
	    include IA_ROOT.'/addons/zh_jysp/SendCode.php';
	    $res=pdo_get('zh_jysp_system',array('uniacid'=>$_W['uniacid']));
	    // var_dump($res);die;
	    $appkey=$res['appkey'];//聚合appkey
	    $phone=$_GPC['phone'];//接过来的手机号
	    $tpl_id=$res['tpl_id'];//短信模板id
	    // $str = '3456789abdefghjkmnpswqxyrt';//验证码字符串
        // $code = substr(str_shuffle($str),0,6);
        $code = $_GPC['code'];
	    $tpl_value=urlencode('#code#'."=".$code);//模板变量
	    // $tpl_value=urlencode($res['tpl_value']);//模板变量
	    if (empty($_GPC['phone'])) {
	    	echo json_encode(array('msg'=>'手机号不能为空','code'=>'500'),320);exit();
	    }
	    if (!preg_match('/^1[34578]\d{9}$/',$phone)) {
	    	echo json_encode(array('msg'=>'请输入正确的手机号','code'=>'501'),320);exit();
	    }
	    $results = new SendCode($appkey,$phone,$tpl_id,$tpl_value);
	    $return=$results->code();
	    if($return){
		    $result = json_decode($return,true);
		    $error_code = $result['error_code'];
		    if($error_code == 0){
		        //状态为0，说明短信发送成功
		        // pdo_insert('snx_code',array('code'=>$code,'phone'=>$phone,'create_time'=>date('Y-m-d H:i:s',time())));
		        echo json_encode(array('msg'=>'发送成功','code'=>$result['result']['sid']),320);exit();
		    }else{
		        //状态非0，说明失败
		        $msg = $result['reason'];
		        echo json_encode(array('msg'=>$msg,'code'=>$error_code),320);exit();
		    }
		}else{
		    //返回内容异常，以下可根据业务逻辑自行修改
		    echo "请求发送短信失败";
		}
	}



	/**
	*用户注册
	**/

	public function doPageRegister(){
		global $_GPC,$_W;
		$data['name'] = $_GPC['name'];
		$data['phone'] = $_GPC['phone'];
		$data['password'] = md5(md5($_GPC['password']));
		$data['create_time'] = date('Y-m-d H:i:s',time());
		$data['uniacid'] = $_W['uniacid'];
		$data['state'] = 1;
		$data['openid']=$_GPC['openid'];
		$data['img']=$_GPC['img'];
		$data['nickname']=$_GPC['nickname'];
	    $res = pdo_getall('zh_jysp_teacher',array('phone'=>$_GPC['phone']),array('phone'));
	    if ($res) {
	    	echo json_encode(array('msg'=>'该手机号已被注册','code'=>'505'),320);exit();
	    }else{
	    	$row = pdo_insert('zh_jysp_teacher',$data);
			if ($row) {
				echo json_encode(array('msg'=>'注册成功，等待审核','code'=>'200'),320);exit();
	        }else{
				echo json_encode(array('msg'=>'注册失败','code'=>'506'),320);exit();
			}
	    }
	}
}