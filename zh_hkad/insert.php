<?php
pdo_query("CREATE TABLE IF NOT EXISTS".tablename('zh_hkad_active')."(
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `content` text NOT NULL,
  `address` varchar(255) NOT NULL,
  `company` varchar(40) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `people` int(11) NOT NULL,
  `money` decimal(11,1) NOT NULL,
  `time` datetime NOT NULL,
  `totime` datetime NOT NULL,
  `uniacid` varchar(30) NOT NULL,
  `tip` text NOT NULL,
  `protocol` text NOT NULL,
  `style` text NOT NULL COMMENT '风格',
  `home` text NOT NULL COMMENT '户型'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;"
);

pdo_query("CREATE TABLE IF NOT EXISTS".tablename('zh_hkad_activebanner')."(
  `id` int(11) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '主键id',
  `title` varchar(30) NOT NULL,
  `img` varchar(100) NOT NULL,
  `link` varchar(255) NOT NULL,
  `sort` tinyint(1) NOT NULL,
  `time` datetime NOT NULL,
  `uniacid` varchar(30) NOT NULL,
  `state` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;"
);
pdo_query("CREATE TABLE IF NOT EXISTS".tablename('zh_hkad_register')."(
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `openid` varchar(50) NOT NULL COMMENT 'openid',
  `img` varchar(255) NOT NULL COMMENT '头像',
  `nickname` varchar(30) NOT NULL COMMENT '昵称',
  `name` varchar(16) NOT NULL COMMENT '真实姓名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `phone` varchar(11) NOT NULL COMMENT '手机号',
  `address` varchar(255) NOT NULL COMMENT '地址',
  `detail_address` varchar(255) NOT NULL COMMENT '详情地址',
  `state` tinyint(1) unsigned NOT NULL COMMENT '状态：1.已报名',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `uniacid` varchar(30) NOT NULL COMMENT '所属模块',
  `agree` tinyint(1) NOT NULL COMMENT '1、同意协议',
  `style` varchar(20) NOT NULL COMMENT '风格',
  `home` varchar(20) NOT NULL COMMENT '户型',
  `area` varchar(10) NOT NULL COMMENT '面积',
  `imgs` text NOT NULL COMMENT '多图片'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;"
);

pdo_query("CREATE TABLE IF NOT EXISTS".tablename('zh_hkad_system')."(
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `appid` varchar(100) NOT NULL COMMENT 'appid',
  `appsecret` varchar(200) NOT NULL COMMENT 'appsecret',
  `uniacid` varchar(50) NOT NULL COMMENT '所属模块',
  `appkey` varchar(50) NOT NULL COMMENT '短信appkey',
  `tpl_id` varchar(20) NOT NULL COMMENT '短信模板id',
  `pt_name` varchar(30) NOT NULL COMMENT '平台名称',
  `pt_tel` varchar(20) NOT NULL COMMENT '平台电话',
  `mac_id` varchar(20) NOT NULL COMMENT '商户号',
  `apikey` varchar(100) NOT NULL COMMENT '商户密钥',
  `apiclient_cert` text NOT NULL COMMENT 'apiclient_cert',
  `apiclient_key` text NOT NULL COMMENT 'apiclient_key',
  `copy` varchar(50) NOT NULL COMMENT '底部版权文字',
  `img` varchar(100) NOT NULL COMMENT '底部版权图标',
  `link` varchar(255) NOT NULL COMMENT '底部版权链接',
  `sell_order` varchar(100) NOT NULL COMMENT '外卖下单通知',
  `sell_getorder` varchar(100) NOT NULL COMMENT '外卖接单通知',
  `sell_reorder` varchar(100) NOT NULL COMMENT '拒绝接单通知',
  `sell_remoney` varchar(100) NOT NULL COMMENT '退款通知',
  `applyin` varchar(100) NOT NULL COMMENT '入住申请通知',
  `verifyin` varchar(100) NOT NULL COMMENT '入住审核通知',
  `tel` varchar(20) NOT NULL COMMENT '客服电话',
  `address` varchar(50) NOT NULL COMMENT '坐标地址',
  `latlng` varchar(50) NOT NULL COMMENT '坐标'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;"
);

?>