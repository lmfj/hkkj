<?php 
    class SendCode{
        protected $appkey;
        protected $phone;
        protected $tpl_id;
        protected $tpl_value;
         function __construct($appkey, $phone, $tpl_id, $tpl_value) {
         $this->appkey = $appkey;
         $this->mobile = $phone;
         $this->tpl_id = $tpl_id;
         $this->tpl_value = $tpl_value;
     }

     public function code(){
        $sendUrl = "http://v.juhe.cn/sms/send";
        $smsConf = array(
            'key'   => $this->appkey, //您申请的APPKEY
            'mobile'    => $this->mobile, //接受短信的用户手机号码
            'tpl_id'    => $this->tpl_id, //您申请的短信模板ID，根据实际情况修改
            'tpl_value' =>$this->tpl_value //您设置的模板变量，根据实际情况修改
        );

        $content = $this->juhecurl($sendUrl,$smsConf,1);
        return $content;
     }
     /**
     * 请求接口返回内容
     * @param  string $url [请求的URL地址]
     * @param  string $params [请求的参数]
     * @param  int $ipost [是否采用POST形式]
     * @return  string
     */
     private static function juhecurl($url,$params=false,$ispost=0){
        $httpInfo = array();
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
        curl_setopt( $ch, CURLOPT_USERAGENT , 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22' );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 30 );
        curl_setopt( $ch, CURLOPT_TIMEOUT , 30);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
        if( $ispost )
        {
            curl_setopt( $ch , CURLOPT_POST , true );
            curl_setopt( $ch , CURLOPT_POSTFIELDS , $params );
            curl_setopt( $ch , CURLOPT_URL , $url );
        }
        else
        {
            if($params){
                curl_setopt( $ch , CURLOPT_URL , $url.'?'.$params );
            }else{
                curl_setopt( $ch , CURLOPT_URL , $url);
            }
        }
        $response = curl_exec( $ch );
        if ($response === FALSE) {
            //echo "cURL Error: " . curl_error($ch);
            return false;
        }
        $httpCode = curl_getinfo( $ch , CURLINFO_HTTP_CODE );
        $httpInfo = array_merge( $httpInfo , curl_getinfo( $ch ) );
        curl_close( $ch );
        return $response;
    }
    }
 ?>