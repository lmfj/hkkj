<?php
global $_GPC, $_W;
$GLOBALS['frames'] = $this->getMainMenu();
$certfile = IA_ROOT . "/addons/zh_hkad/cert/" . 'apiclient_cert_' . $_W['uniacid'] . '.pem';
$keyfile = IA_ROOT . "/addons/zh_hkad/cert/" . 'apiclient_key_' . $_W['uniacid'] . '.pem';
$item=pdo_get('zh_hkad_system',array('uniacid'=>$_W['uniacid']));

    if(checksubmit('submit')){
    		if($_GPC['apiclient_cert']){
            file_put_contents($certfile, trim($_GPC['apiclient_cert']));
            $data['apiclient_cert']=$_GPC['apiclient_cert'];
	        }
	        if($_GPC['apiclient_key']){
	             file_put_contents($keyfile, trim($_GPC['apiclient_key']));
	            $data['apiclient_key']=$_GPC['apiclient_key']; 
	        }
            $data['mac_id']=$_GPC['mac_id'];
            $data['apikey']=$_GPC['apikey'];
            if($_GPC['mac_id']==''){
                message('商户号不能为空!','','error'); 
            }
            if($_GPC['apikey']==''){
                message('密钥不能为空!','','error'); 
            }
            if(empty($item)){                
                $res=pdo_insert('zh_hkad_system',$data);
                if($res){
                    message('添加成功',$this->createWebUrl('pay',array()),'success');
                }else{
                    message('添加失败','','error');
                }
            }else{
                $res = pdo_update('zh_hkad_system', $data, array('id' => $_GPC['id']));
                if($res){
                    message('编辑成功',$this->createWebUrl('pay',array()),'success');
                }else{
                    message('编辑失败','','error');
                }
            }
    }
include $this->template('web/pay');