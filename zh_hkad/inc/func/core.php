<?php
defined('IN_IA') or exit ('Access Denied');

class Core extends WeModuleSite
{
  
    public function getMainMenu()
    {
        global $_W, $_GPC;

        $do = $_GPC['do'];
        $navemenu = array();
        $cur_color = ' style="color:#d9534f;" ';
            // $navemenu[0] = array(
            //     'title' => '<a href="index.php?c=site&a=entry&op=display&do=gaikuangdata&m=zh_jypx" class="panel-title wytitle" id="yframe-0"><icon style="color:#8d8d8d;" class="fa fa-cubes"></icon>  数据统计</a>',
            //     'items' => array(
            //        0 => $this->createMainMenu('数据概况 ', $do, 'gaikuangdata', ''),
            //        // 0 => $this->createMainMenu('门店回收站 ', $do, 'yg4', ''),
            //    )
            // );
            $navemenu[3] = array(
                'title' => '<a href="index.php?c=site&a=entry&op=display&do=active&m=zh_hkad" class="panel-title wytitle" id="yframe-3"><icon style="color:#8d8d8d;" class="fa fa-pencil-square-o"></icon> 活动管理</a>',
                'items' => array(
                    0 => $this->createMainMenu('活动广告图', $do, 'activebanner', ''),
                    1 => $this->createMainMenu('基础设置', $do, 'active', ''),
                    // 1 => $this->createMainMenu('活动详情', $do, 'activedetail', ''),
                    2 => $this->createMainMenu('报名列表', $do, 'activelist', ''),
                )
            );
            $navemenu[14] = array(
                'title' => '<a href="index.php?c=site&a=entry&op=display&do=peiz&m=zh_hkad" class="panel-title wytitle" id="yframe-14"><icon style="color:#8d8d8d;" class="fa fa-cog"></icon>  系统设置</a>',
                'items' => array(
                    1 => $this->createMainMenu('公众号配置', $do, 'peiz', ''),
                    2 => $this->createMainMenu('支付配置', $do, 'pay', ''),
                )
            );
        return $navemenu;
    }

    // public function getNaveMenu()
    // {
    //     global $_W, $_GPC;
    //     $do = $_GPC['do'];
    //     $navemenu = array();
    //     $cur_color = '#8d8d8d';
    //     $navemenu[2] = array(
    //         'title' => '<a href="zhjzcus.php?c=site&a=entry&do=jzgoods&m=zh_zhjz" class="panel-title wytitle" id="yframe-1"><icon style="color:#8d8d8d;" class="fa fa-user"></icon>商品管理</a>',
    //         'items' => array(
    //              0 => $this->createSubMenu('商品管理 ', $do, 'jzgoods', 'fa-angle-right', $cur_color,''),
    //         )
    //     );

    //     $navemenu[5] = array(
    //         'title' => '<a href="zhjzcus.php?c=site&a=entry&do=jzorder&m=zh_zhjz" class="panel-title wytitle" id="yframe-5"><icon style="color:#8d8d8d;" class="fa fa-comment-o"></icon>  订单管理</a>',
    //         'items' => array(
    //             0 => $this->createSubMenu('订单列表', $do, 'jzorder', 'fa-angle-right', $cur_color,''),
    //         )
    //     );
    //     return $navemenu;
    // }
    function createCoverMenu($title, $method, $op, $icon = "fa-image", $color = '#d9534f')
    {
        global $_GPC, $_W;
        $cur_op = $_GPC['op'];
        $color = ' style="color:'.$color.';" ';
        return array('title' => $title, 'url' => $op != $cur_op ? $this->createWebUrl($method, array('op' => $op)) : '',
            'active' => $op == $cur_op ? ' active' : '',
            'append' => array(
                'title' => '<i class="fa fa-angle-right"></i>',
            )
        );
    }

    function createMainMenu($title, $do, $method, $icon = "fa-image", $color = '')
    {
        $color = ' style="color:'.$color.';" ';

        return array('title' => $title, 'url' => $do != $method ? $this->createWebUrl($method, array('op' => 'display')) : '',
            'active' => $do == $method ? ' active' : '',
            'append' => array(
                'title' => '<i '.$color.' class="fa fa-angle-right"></i>',
            )
        );
    }

  /*  function createSubMenu($title, $do, $method, $icon = "fa-image", $color = '#d9534f', $storeid)
    {
        $color = ' style="color:'.$color.';" ';
        $url = $this->createWebUrl($method, array('op' => 'display', 'storeid' => $storeid));
        if ($method == 'stores') {
            $url = $this->createWebUrl('stores', array('op' => 'post', 'id' => $storeid, 'storeid' => $storeid));
        }

        return array('title' => $title, 'url' => $do != $method ? $url : '',
            'active' => $do == $method ? ' active' : '',
            'append' => array(
                'title' => '<i class="fa '.$icon.'"></i>',
            )
        );
    }

*/
    function createWebUrl2($do, $query = array()) {
        $query['do'] = $do;
        $query['m'] = strtolower($this->modulename);
      
        return $this->wurl('site/entry', $query);
    }

    // function wurl($segment, $params = array()) {
      
    //     list($controller, $action, $do) = explode('/', $segment);
    //     $url = './zhjzcus.php?';
    //     if (!empty($controller)) {
    //         $url .= "c={$controller}&";
    //     }
    //     if (!empty($action)) {
    //         $url .= "a={$action}&";
    //     }
    //     if (!empty($do)) {
    //         $url .= "do={$do}&";
    //     }
    //     if (!empty($params)) {
    //         $queryString = http_build_query($params, '', '&');
    //         $url .= $queryString;
    //     }
    //     return $url;
    // }

        function createSubMenu($title, $do, $method, $icon = "fa-image", $color = '#d9534f', $storeid)
    {
        $color = ' style="color:'.$color.';" ';
        $url = $this->createWebUrl2($method, array('op' => 'display', 'storeid' => $storeid));
        if ($method == 'stores2') {
            $url = $this->createWebUrl2('stores2', array('op' => 'post', 'id' => $storeid, 'storeid' => $storeid));
        }



        return array('title' => $title, 'url' => $do != $method ? $url : '',
            'active' => $do == $method ? ' active' : '',
            'append' => array(
                'title' => '<i class="fa '.$icon.'"></i>',
            )
        );
    }
    public function getStoreById($id)
    {
        $store = pdo_fetch("SELECT * FROM " . tablename('wpdc_store') . " WHERE id=:id LIMIT 1", array(':id' => $id));
        return $store;
    }


    public function set_tabbar($action, $storeid)
    {
        $actions_titles = $this->actions_titles;
        $html = '<ul class="nav nav-tabs">';
        foreach ($actions_titles as $key => $value) {
            if ($key == 'stores') {
                $url = $this->createWebUrl('stores', array('op' => 'post', 'id' => $storeid));
            } else {
                $url = $this->createWebUrl($key, array('op' => 'display', 'storeid' => $storeid));
            }

            $html .= '<li class="' . ($key == $action ? 'active' : '') . '"><a href="' . $url . '">' . $value . '</a></li>';
        }
        $html .= '</ul>';
        return $html;
    }
}